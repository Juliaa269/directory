import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeesComponent } from '../components/employees/employees.component';
import { EmployeeCardComponent } from '../components/employee-card/employee-card.component';
import { UpdateEmployeeComponent } from '../components/update-employee/update-employee.component';
import { CreateEmployeeComponent } from '../components/create-employee/create-employee.component';
import { AuthorizationComponent } from '../components/authorization/authorization.component';
import { RegistrationComponent } from '../components/registration/registration.component';
import { EmployeesGuard } from '../guards/employees.guard';
import { BrowserModule } from '@angular/platform-browser';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/register',
    pathMatch: 'full'
  },
  {
    path: 'employees',
    component: EmployeesComponent,
    canActivate: [EmployeesGuard]
  },
  {
    path: 'card/:id',
    component: EmployeeCardComponent
  },
  {
    path: 'card/:id/update',
    component: UpdateEmployeeComponent
  },
  {
    path: 'employees/create',
    component: CreateEmployeeComponent
  },
  {
    path: 'login',
    component: AuthorizationComponent
  },
  {
    path: 'register',
    component: RegistrationComponent
  },
  {
    path: '**',
    redirectTo: 'login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), BrowserModule],
  exports: [RouterModule],
  providers: [EmployeesGuard]
})
export class AppRoutingModule {

}
