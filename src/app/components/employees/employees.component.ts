import { Component, OnInit } from '@angular/core';
import { Employee } from '../../helpers/employee';
import { EmployeeService } from '../../services/employee.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MaterialModule } from '../../modules/material-module';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})

export class EmployeesComponent implements OnInit {

  // public employees: [] = [];

  employee: Employee | undefined;
  selectedEmployee?: Employee;

  title = 'Workers Page';
  response: any;

  employees: Employee[] = [];
  avatar: any = "../assets/images/employee.jpg";

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }
  id!: number;

  showEmployees: boolean = false;

  constructor(
    private employeeService: EmployeeService,
    private http: HttpClient,
    private material: MaterialModule
    ) { }

  ngOnInit(): void {
    this.showEmployees =! this.showEmployees;
    this.employeeService.getEmployees()
    .subscribe(data => this.employees = data.data)
  }

  onSelect(employee: Employee): void {
    this.selectedEmployee = employee;
  }

  

}
