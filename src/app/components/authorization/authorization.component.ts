import { Component, OnInit } from '@angular/core';
import { FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthorizationService } from '../../services/authorization.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.scss']
})
export class AuthorizationComponent implements OnInit {

  loginForm!: FormGroup;

  title = 'Authorization Page';

  constructor(
    private authService: AuthorizationService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl(null, [
        Validators.required,
        Validators.email,
        Validators.minLength(6)
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(3)
      ])
    })
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    } 

    this.authService.login({email: this.loginForm.value.email, password: this.loginForm.value.password})
    .pipe(
      map(token => this.router.navigate(['employees']))
    ).subscribe();
  }

}
