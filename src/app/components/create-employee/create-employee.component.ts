import { Component, OnInit } from '@angular/core';
import { Employee } from '../../helpers/employee';
import { EmployeeService } from '../../services/employee.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.scss']
})
export class CreateEmployeeComponent implements OnInit {

  employee?: Employee;
  employees: Employee[] = [];
  
  constructor(
    private employeeService: EmployeeService,
    private location: Location
  ) { }

  ngOnInit(): void {
  }

  add(first_name: string, position: string): void {
    first_name = first_name.trim();
    position = position.trim();
    if (!first_name && !position) { return;}
    this.employeeService.addEmployee( { first_name, position } as Employee)
      .subscribe(() => this.goBack())
  }

  goBack(): void {
    this.location.back();
  }
}
