import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthorizationService, User } from '../../services/authorization.service';
import { map } from 'rxjs/operators';

class CustomValidators {
  static passwordContainsNumber(control: AbstractControl): ValidationErrors {
    const regex = /\d/;

    if(regex.test(control.value) && control.value !== null) {
      return null!
    } else {
      return { passwordInvalid: true };
    }
  }
  
  static passwordMatch (control: AbstractControl): ValidationErrors {
    const password = control.get('password')?.value;
    const confirmPassword = control.get('confirmPassword')?.value;

    if((password === confirmPassword) && (password !== null && confirmPassword !== null)) {
      return null!;
    } else {
      return { passwordNotMatching: true }
    }
  }
}

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  
  registerForm!: FormGroup;
  title = 'Registration Page';

  constructor(
    private authorizationService: AuthorizationService,
    private formBuilder: FormBuilder,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      email: [null, [Validators.minLength(6), Validators.required]],
      password: [null, [Validators.minLength(3), Validators.required, CustomValidators.passwordContainsNumber]],
      confirmPassword: [null, [Validators.minLength(3), Validators.required]]
    },
    {
      validators: CustomValidators.passwordMatch
    })
  }

  onSubmit() {
    if(this.registerForm.invalid) {
      return;
    }
    console.log(this.registerForm.value);
    this.authorizationService.register(this.registerForm.value).pipe(
      map(user => this.router.navigate(['login']))
    ).subscribe();
  }
}
