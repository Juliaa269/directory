import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, JsonpClientBackend } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';

export interface LoginForm {
  email: string;
  password: string;
}

export interface User {
  email: string;
  password: string;
  confirmPassword: string;
}

@Injectable({
  providedIn: 'root'
})

export class AuthorizationService {

  private authUrl = 'https://reqres.in/api/login';

  constructor(
    private http: HttpClient
  ) { }

  login(loginForm: LoginForm) {
    return this.http.post<any>(this.authUrl, {email: loginForm.email, password: loginForm.password})
      .pipe(
        map((token) => {
          console.log(token);
          localStorage.setItem('token', JSON.stringify(token));
          // }
          return token;
        })
      )
  }

  register(user: any) {
    return this.http.post<any>('https://reqres.in/api/register', user).pipe(
      map(user => user)
    )
  }
}
