import { Injectable } from '@angular/core';
import { Employee } from '../helpers/employee';
import { Observable, of } from 'rxjs';
import { Pageable } from '../helpers/pageable';

import { catchError, map, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class EmployeeService {

  private url = 'https://reqres.in/api/users?page=';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  constructor(
    private http: HttpClient
  ) { }
  
  getEmployees(): Observable<Pageable>{
    return this.http.get<Pageable>(this.url);
  }

  // PUT: update employees from the server
  updateEmployee(employee: Employee): Observable<any> {
    return this.http.put(this.url, employee, this.httpOptions)
    .pipe(
      catchError(this.handleError<Employee[]>('updateEmployee', []))
    )
  }

  // DELETE: delete the employee from the server
  deleteEmployee(id: number): Observable<any> {
    const url = `${'https://reqres.in/api/users'}/${id}`;
    return this.http.delete<any>(url, this.httpOptions)
    .pipe(
      catchError(this.handleError<any>('deleteEmployee'))
    )
  }

  // POST: add a new employee to the server
  addEmployee(employee: Employee): Observable<any> {
    return this.http.post<Employee>('https://reqres.in/api/users', employee, this.httpOptions).pipe(
      catchError(this.handleError<Employee>('addEmployee'))
    )
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  getEmployee(id: number): Observable<any> {
    const url = `${'https://reqres.in/api/users'}/${id}`;
    return this.http.get<any>(url).pipe(
      catchError(this.handleError<any>(`getEmployee id=${id}`))
    )
  }

}
