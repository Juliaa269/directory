export interface Employee {
    id: number;
    avatar: any;
    first_name: string;
    last_name: string;
    email: string;
    position: string;
}
